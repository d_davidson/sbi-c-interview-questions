﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorInterviewTest
{
    /// <summary>
    /// Palindrome: a word, phrase, or sequence that reads the same backward as forward.
    /// Allowances may be made for adjustments to capital letters, punctuation, and word dividers
    /// </summary>
    public class Palindrome
    {
        public Palindrome(string checkWordOrPhrase)
        {

        }
        public bool IsPalindrome()
        {
            return false;
        }
    }

    [TestClass]
    public class PalindromeTest
    {
        [TestMethod]
        public void SingleWord()
        {
            var testClass = new Palindrome("tattarrattat");
            Assert.IsTrue(testClass.IsPalindrome());
        }
        [TestMethod]
        public void MultiWord()
        {
            var testClass = new Palindrome("lion oil");
            Assert.IsTrue(testClass.IsPalindrome());
        }
        [TestMethod]
        public void MultiWordWithCases()
        {
            var testClass = new Palindrome("Too hot to hoot");
            Assert.IsTrue(testClass.IsPalindrome());
        }
        [TestMethod]
        public void MultiWordWithCasesAndPunctuation()
        {
            var testClass = new Palindrome("A man, a plan, a canal: Panama.");
            Assert.IsTrue(testClass.IsPalindrome());
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullWord()
        {
            var testClass = new Palindrome(null);
        }



    }
}
