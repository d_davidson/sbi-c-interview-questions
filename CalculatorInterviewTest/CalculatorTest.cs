﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorInterviewTest
{
    public class Calculator
    {
        public int Add(int first, int second)
        {
            return 0;
        }

        public int Subtract(int first, int second)
        {
            return 0;
        }

        public int Multiply(int first, int second)
        {
            return 0;
        }

        public int Divide(int first, int second)
        {
            return 0;
        }
    }

    [TestClass]
    public class CalculatorTest
    {
        [TestMethod]
        public void TestAddition()
        {
            Calculator calculator = new Calculator();
            Assert.AreEqual(3, calculator.Add(1, 2));
        }

        [TestMethod]
        public void TestSubtraction()
        {
            Calculator calculator = new Calculator();
            Assert.AreEqual(7, calculator.Subtract(9, 2));
        }

        [TestMethod]
        public void TestMultiplication()
        {
            Calculator calculator = new Calculator();
            Assert.AreEqual(12, calculator.Multiply(4, 3));
        }

        [TestMethod]
        public void TestDivision()
        {
            Calculator calculator = new Calculator();
            Assert.AreEqual(2, calculator.Divide(10, 5));
        }
    }
}
