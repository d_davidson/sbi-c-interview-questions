﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorInterviewTest
{
    /// <summary>
    /// The aim of this test is to implement the FizzBuzz problem.
    /// FizzBuzz is defined for the "natural numbers" (numbers greater than zero) as:
    /// When divisible by 3, you should return "Fizz".
    /// When divisible by 5, you should return "Buzz".
    /// When divisible by both 3 and 5, you should return "FizzBuzz".
    /// When divisible by neither, you should return the number itself.
    /// </summary>
    public class FizzBuzz
    {
        public FizzBuzz(int index)
        {
        }

    }

    [TestClass]
    public class FizzBuzzTest
    {

        [TestMethod]
        public void TestInputOne()
        {
            FizzBuzz fizzbuzz = new FizzBuzz(1);
            Assert.AreEqual("1", fizzbuzz.ToString());
        }

        [TestMethod]
        public void TestInputThree()
        {
            FizzBuzz fizzbuzz = new FizzBuzz(3);
            Assert.AreEqual("Fizz", fizzbuzz.ToString());
        }

        [TestMethod]
        public void TestInputFive()
        {
            FizzBuzz fizzbuzz = new FizzBuzz(5);
            Assert.AreEqual("Buzz", fizzbuzz.ToString());
        }

        [TestMethod]
        public void TestInputFifteen()
        {
            FizzBuzz fizzbuzz = new FizzBuzz(15);
            Assert.AreEqual("FizzBuzz", fizzbuzz.ToString());
        }

        [TestMethod]
        public void TestInputOneHundredAndOne()
        {
            FizzBuzz fizzbuzz = new FizzBuzz(101);
            Assert.AreEqual("101", fizzbuzz.ToString());
        }

        [TestMethod]
        public void TestInputNinetyNine()
        {
            FizzBuzz fizzbuzz = new FizzBuzz(99);
            Assert.AreEqual("Fizz", fizzbuzz.ToString());
        }

        [TestMethod]
        public void TestInputOneHundred()
        {
            FizzBuzz fizzbuzz = new FizzBuzz(100);
            Assert.AreEqual("Buzz", fizzbuzz.ToString());
        }

        [TestMethod]
        public void TestInputOneHundredAndFive()
        {
            FizzBuzz fizzbuzz = new FizzBuzz(105);
            Assert.AreEqual("FizzBuzz", fizzbuzz.ToString());
        }
    }
}
