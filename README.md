# README #

SBI C# Code Examples

### What is this repository for? ###

* Quick summary
    * Please make all tests pass and submit you results.
* Version
    * 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
    * You will need Visual Studio.  If you need to download please grab the [Community Edition](https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx)
    * Open the CalculatorInterviewTest.sln and run all test. 
    * Make all the tests pass
* Configuration
    * No configuration needed
* Dependencies
    * [Visual Studio](https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx)
* Database configuration
    * None
* How to run tests
    * Open Solution (CalculatorInterviewTest.sln)
    * Open the Test Explorer Test > Windows > Test Explorer
    * Press ctrl + r, a to run all tests
* Deployment instructions
    * None.  This project is self contained.

### Contribution guidelines ###

* Writing tests
    * We have written all the tests.  But there might be a few edge cases we missed.
* Code review
    * We'll go over it in person.
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
    * Darryl Davidson | [darryl@sbiteam.com](mailto:darryl@sbiteam.com)
* Other community or team contact
    * None yet